# Universidade Federal de Goiás

## Chat Simples utilizando AJAX

**Rafael Ratacheski de Sousa Raulino** - *rafaelratacheski@gmail.com*

.

* Bate-papo simples entre vários usuários através de um servidor node js.

# Manual do desenvolvedor

Para executar o arquivo javascript é necessário baixar o nodejs.(Para isso rode o comando no linux: *'sudo apt-get install nodejs-legacy'*)

Após isso basta rodar o comando *'node app.js'*. 

Para testar o servidor acessar em seu navegador o endereço http://localhost:3000/



