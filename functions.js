var Message;
Message = function (arg) {
    this.text = arg.text, this.message_side = arg.message_side;
    this.draw = function (_this) {
        return function () {
            var message;
            message = $($('.message_template').clone().html());
            message.addClass(_this.message_side).find('.text').html(_this.text);
            $('.messages').append(message);
            return setTimeout(function () {
                return message.addClass('appeared');
            }, 0);
        };
    }(this);
    return this;
};

$(function () {
    var getMessageText, message_side, sendMessage;
    message_side = 'right';
    var message_array = [];
    getMessageText = function () {
        var message_input;

        message_input = $('.message_sender').val() + ": " + $('.message_input').val();
        return message_input;
    };

    sendMessage = function (text) {
        var messages, message;
        if (text.trim() === '') {
            return;
        }
        var ajax = new XMLHttpRequest();
        ajax.open("POST", "http://localhost:3000/gravar_mensagem", true);
        ajax.setRequestHeader("Content-type", "application/json");
        var nome = $('.message_sender').val();
        var mensagem = $('.message_input').val();
        var send = { "timestamp": Date.now(), "nick": nome, "msg": mensagem };
        ajax.send(JSON.stringify(send));
        $('.message_input').val('');
    };

    //chama o sendMessage se clicar no botão
    $('.send_message').click(function (e) {
        return sendMessage(getMessageText());
    });

    //chama o sendMessage se teclar Enter
    $('.message_input').keyup(function (e) {
        if (e.which === 13) {
            return sendMessage(getMessageText());
        }
    });

    getMessage = function () {
        if ($('.message_sender').val() !== ('')) {
            document.getElementById("sender_wrapper").style.border="1px solid #bcbdc0";
            setInterval(function () {
                var ajax = new XMLHttpRequest();
                ajax.open("GET", "http://localhost:3000/obter_mensagem", true);
                ajax.send();
                ajax.onreadystatechange = function () {
                    if (ajax.readyState == 4 && ajax.status == 200) {
                        var data = JSON.parse(ajax.responseText);
                        var text = '';
                        var name = $('.message_sender').val();
                        messages = $('.messages');
                        data.forEach(function (item) {
                            message_side = 'left';
                            text = '[' + timeConverter(parseInt(item.timestamp)) + '] <b>' + item.nick + '</b>: ' + item.msg + '</br>';
                            var name_comparator = text.substr(0,text.indexOf("</b>:"));    
                            if ((name_comparator.indexOf(name) > -1) && ($('.message_sender').val() !== (''))) {
                                message_side = 'right';
                            }
                            message = new Message({
                                text: text,
                                message_side: message_side
                            });

                            if (message_array.findIndex(k => k.text == message.text) === -1) {
                                message_array.push(message);
                                message.draw();
                                return messages.animate({ scrollTop: messages.prop('scrollHeight') }, 300);
                            }
                        });
                    }
                }
            }, 1000);
        } else {
            document.getElementById("sender_wrapper").style.border="2px solid #a3d063";
            $('.message_sender').focus();
        }
    };
});

function timeConverter(timestamp){
    var a = new Date(timestamp);
    var months = ['Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set','Out','Nov','Dez'];
    var year = a.getFullYear();
    var month = months[a.getMonth()];
    var date = validaZeros(a.getDate());
    var hour = validaZeros(a.getHours());
    var min = validaZeros(a.getMinutes());
    var sec = validaZeros(a.getSeconds());
    var time = date + '/' + month + '/' + year + ' ' + hour + ':' + min + ':' + sec ;
    return time;
}

function validaZeros (value){
    if(value < 10){ 
        value = "0"+value; 
    }
    return value;
}


